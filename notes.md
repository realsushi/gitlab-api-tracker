# Gitlab API for tracking

## Goal
Provide custom tracking metrics of the work done

## API endpoints to fetch 
- https://docs.gitlab.com/ee/api/commits.html

## git cli data
notice the abreviation `git log  --date=iso-local --pretty=tformat:'%cd %h' --abbrev-commit`
