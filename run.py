import os
import time
import subprocess

from fastapi import FastAPI
import requests

app = FastAPI()

BITBUCKET_API = os.environ.get("BITBUCKET_API")
BITBUCKET_WORKSPACE = os.environ.get("BITBUCKET_WORKSPACE")
BITBUCKET_USERNAME = os.environ.get("BITBUCKET_USERNAME")
BITBUCKET_TOKEN = os.environ.get("BITBUCKET_TOKEN")
BB_PROJECT = os.environ.get("BB_PROJECT")

GITHUB_API = os.environ.get("GITHUB_API")
GITHUB_ORG = os.environ.get("GITHUB_ORG")
GITHUB_TOKEN = os.environ.get("GITHUB_TOKEN")

GITLAB_API = os.environ.get("GITLAB_API")
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN")

####################
# Bitbucket routes #
####################
@app.get("/bitbucket/{BB_PROJECT}/repos")
def get_bitbucket_repos(BB_PROJECT: str):
    # Get repos from Bitbucket
    response = requests.get(
        f"{BITBUCKET_API}/repositories/{BITBUCKET_WORKSPACE}?q=project.key=\"{BB_PROJECT}\"",
        auth=(BITBUCKET_USERNAME, BITBUCKET_TOKEN)
    )
    return response.json()




##################
# GitHub routes  #
##################

@app.get("/repos")
def get_repos():
    # Get repos from GitHub
    response = requests.get(f"{GITHUB_API}/orgs/{GITHUB_ORG}/repos",
                            headers={"Authorization": f"token {GITHUB_TOKEN}"})
    return response.json()






@app.get("/teams")
def get_teams():
    # Get teams from GitHub
    response = requests.get(f"{GITHUB_API}/orgs/{GITHUB_ORG}/teams",
                            headers={"Authorization": f"token {GITHUB_TOKEN}"})
    return response.json()

@app.post("/teams/{team_name}")
def create_team(team_name: str):
    # Create a team on GitHub
    response = requests.post(f"{GITHUB_API}/orgs/{GITHUB_ORG}/teams",
                            headers={"Authorization": f"token {GITHUB_TOKEN}"},
                            json={"name": team_name})
    return response.json()


from typing import List

@app.delete("/delete-repos")
async def delete_repos(repos: List[str]):
    for repo_name in repos:
        response = requests.delete(
            f"{GITHUB_API}/repos/{GITHUB_ORG}/{repo_name}",
            headers={"Authorization": f"token {GITHUB_TOKEN}"}
        )

        if response.status_code != 204:
            return {"error": f"Failed to delete repository {repo_name} on GitHub"}

    return {"message": "Repositories deleted successfully"}

##################
# GitLab routes  #
##################

@app.get("/gitlab/projects")
def get_gitlab_projects():
    # Get projects from GitLab
    response = requests.get(f"{GITLAB_API}/projects",
                            headers={"Authorization": f"token {GITLAB_TOKEN}"})
    return response.json()

@app.post("/gitlab/projects")
def create_gitlab_project():
    # Create a project on GitLab
    response = requests.post(f"{GITLAB_API}/projects",
                             headers={"Authorization": f"token {GITLAB_TOKEN}"},
                             json={"name": "test"})
    return response.json()

##################
# migration routes #
##################

@app.get("/migrate1")
def migrate_repos():
    # Get repos from Bitbucket
    bitbucket_repos = requests.get(f"{BITBUCKET_API}/repositories/{BITBUCKET_WORKSPACE}",
                                auth=(BITBUCKET_USERNAME, BITBUCKET_TOKEN)).json()['values']
    for bitbucket_repo in bitbucket_repos:
        repo_name = bitbucket_repo['slug']
        
        # Create the repo on GitHub
        github_repo = requests.post(f"{GITHUB_API}/orgs/{GITHUB_ORG}/repos",
                                    headers={"Authorization": f"token {GITHUB_TOKEN}"},
                                    json={"name": repo_name}).json()
        
        # Clone the repo from Bitbucket, add GitHub as remote, and push
        subprocess.run(["git", "clone", bitbucket_repo['links']['clone'][0]['href']])
        subprocess.run(["git", "remote", "add", "github", github_repo['clone_url']], cwd=repo_name)
        subprocess.run(["git", "push", "-u", "github", "master"], cwd=repo_name)
    return {"message": "Migration started"}



@app.post("/migrate/{project_key}")
async def migrate_repos(project_key: str):
    # Get repos from Bitbucket
    bb_response = requests.get(
        f"{BITBUCKET_API}/repositories/{BITBUCKET_WORKSPACE}?q=project.key=\"{project_key}\"",
        auth=(BITBUCKET_USERNAME, BITBUCKET_TOKEN)
    )
    bb_repos = bb_response.json()['values']

    # Check if the team already exists
    team_exists_response = requests.get(
        f"{GITHUB_API}/orgs/{GITHUB_ORG}/teams/{project_key}",
        headers={"Authorization": f"token {GITHUB_TOKEN}"}
    )

    if team_exists_response.status_code == 200:
        # Team already exists
        team_slug = team_exists_response.json()['slug']
    else:
        # Create a team on GitHub
        team_response = requests.post(
            f"{GITHUB_API}/orgs/{GITHUB_ORG}/teams",
            headers={"Authorization": f"token {GITHUB_TOKEN}"},
            json={"name": project_key}
        )

        if team_response.status_code == 201:
            # Team creation successful
            team_slug = team_response.json()['slug']
        else:
            # Team creation failed
            return {"error": f"Failed to create team {project_key} on GitHub"}

    # Loop over Bitbucket repos and push to GitHub
    for repo in bb_repos:
        repo_name = repo['slug']
        # Create a new repo in the GitHub organization
        gh_repo_response = requests.post(
            f"{GITHUB_API}/orgs/{GITHUB_ORG}/repos",
            headers={"Authorization": f"token {GITHUB_TOKEN}"},
            json={"name": repo_name, "private": True}
        )
        
        # Check if the repository creation was successful
        if gh_repo_response.status_code == 201:
            # Find the SSH path for the Bitbucket repo
            ssh_path = next((link['href'] for link in repo['links']['clone'] if link['name'] == 'ssh'), None)
            if not ssh_path:
                return {"error": f"SSH path not found for repo {repo_name}"}

            # Clone the Bitbucket repo
            subprocess.run(['git', 'clone', '--mirror', ssh_path, 'tmp_repo'])

            # Change to the new GitHub repo
            subprocess.run(['cd', 'tmp_repo'])
            subprocess.run(['git', 'remote', 'set-url', '--push', 'origin', gh_repo_response.json()['ssh_url']])

            # Push to the new GitHub repo
            subprocess.run(['git', 'push', '--mirror'])

            # Add the repo to the team
            requests.put(
                f"{GITHUB_API}/orgs/{GITHUB_ORG}/teams/{team_slug}/repos/{GITHUB_ORG}/{repo_name}",
                headers={"Authorization": f"token {GITHUB_TOKEN}"}
            )

            # Clean up temporary repo
            subprocess.run(['cd', '..'])
            subprocess.run(['rm', '-rf', 'tmp_repo'])

            # Add a 500ms delay
            time.sleep(0.5)

        else:
            return {"error": f"Failed to create repository {repo_name} on GitHub"}

    return {"message": "Migration completed"}


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
